import 'dart:async';
import 'dart:io';

//Son para conectar con firebase y cloud
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:uuid/uuid.dart';

class CameraScreen extends StatefulWidget {
  @override
  _CameraScreenState createState() {
    return _CameraScreenState();
  }
}

class _CameraScreenState extends State<CameraScreen> {
  CameraController controller;
  String imagePath;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    controller = CameraController(cameras[0], ResolutionPreset.medium);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text('Schiro sos de la b'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(1.0),
                child: Center(
                  child: _cameraPreviewWidget(),
                ),
              ),
            ),
          ),
          _captureControlRowWidget(),
        ],
      ),
    );
  }

  /// Display the preview from the camera (or a message if the preview is not available).
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Botón para camara',
        style: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.w900,
        ),
      );
    } else {
      return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CameraPreview(controller),
      );
    }
  }

  /// Display the control bar with buttons to take pictures.
  Widget _captureControlRowWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        IconButton(
          icon: const Icon(Icons.camera_alt),
          color: Colors.blue,
          onPressed: controller != null && controller.value.isInitialized
              ? onTakePictureButtonPressed
              : null,
        )
      ],
    );
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          imagePath = filePath;
        });
        if (filePath != null) {
          detectLabels().then((_) {});
        }
      }
    });
  }

  Future<void> detectLabels() async {
    final FirebaseVisionImage visionImage =
        FirebaseVisionImage.fromFilePath(imagePath);
    final ImageLabeler labelDetector = FirebaseVision.instance.imageLabeler();

    final List<ImageLabel> labels =
        await labelDetector.processImage(visionImage);

    List<String> labelTexts = new List();
    for (ImageLabel label in labels) {
      final String text = label.text;

      labelTexts.add(text);
    }

    final String uuid = Uuid().v1();
    final String downloadURL = await _uploadFile(uuid);

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ItemsListScreen()),
    );

    await _addItem(downloadURL, labelTexts);
  }

  Future<void> _addItem(String downloadURL, List<String> labels) async {
    await FirebaseFirestore.instance
        .collection('items')
        .add(<String, dynamic>{'downloadURL': downloadURL, 'labels': labels});
  }

  //Separado para sexto primera y los alumnos que no vienen como
  //Garces, Calvin Klein, Matias Torres y un tal Irigoyen (Existe???)
  Future<void> detectLabels() async {
    final String uuid = Uuid().v1();
    final String nombre = uuid;
    final String tiempoPromoci = uuid;

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ItemsListScreen()),
    );

    await _addPromocion(nombre, tiempoPromoci);
  }

  Future<void> _addPromocion(String nombre, String tiempoPromoci) async {
    await FirebaseFirestore.instance.collection('promociones').add(
        <String, String>{'nombre': nombre, 'tiempoPromoci': tiempoPromoci});
  }

  Future<void> _addDenuncia(String direccion, String estado) async {
    await FirebaseFirestore.instance
        .collection('denuncias')
        .add(<String, String>{'direccion': direccion, 'estado': estado});
  }

  //Separado para Schiro, así no se pierde (Sos de la B)
  /*Future<void> detectLabels() async {

    List<String> labelTexts = new List();
    for (ImageLabel label in labels) {
      final String text = label.text;

      labelTexts.add(text);
    }

    final String uuid = Uuid().v1();
    final String downloadURL = await _uploadFile(uuid);

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ItemsListScreen()),
    );

    await _addUsuario(nombre, apellido, correo);
  }

  Future<void> _addUsuario(String nombre, String apellido, String correo) async{
    await FirebaseFirestore.instance
      .collection('registros')
      .add(<String>{'nombre': nombre,'apellido': apellido,'correo': correo})
  }*/
}
